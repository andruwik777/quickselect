public class QuickSelect {
	public static int select(int[] array, int n) {
        return selectIterative(array, 0, array.length - 1, n);
    }

    private static int selectIterative(int[] array, int left, int right, int n) {
        if(left == right) {
            return array[left];
        }

        for(;;) {
            int pivotIndex = getPivot(left, right);
            pivotIndex = partition(array, left, right, pivotIndex);

            if(n == pivotIndex) {
                return array[n];
            } else if(n < pivotIndex) {
                right = pivotIndex - 1;
            } else {
                left = pivotIndex + 1;
            }
        }
    }

    private static int partition(int[] array, int left, int right, int pivotIndex) {
        int pivotValue = array[pivotIndex];
        swap(array, pivotIndex, right);
        int newPivotIndex = left;
        for(int i = left; i < right; ++i) {
            if(array[i] < pivotValue) {
                swap(array, newPivotIndex, i);
                newPivotIndex++;
            }
        }
        swap(array, right, newPivotIndex);
        return newPivotIndex;
    }

    private static void swap(int[] array, int a, int b) {
        int tmp = array[a];
        array[a] = array[b];
        array[b] = tmp;
    }

    private static int getPivot(int left, int right) {
        return (int) Math.floor(left + right)/2;
    }
}
